\chapter{Regex Play em \textsc{COQ}}

Antes de usar o provador automatico para um dado algoritmo é necessário representá-lo usando as abstrações da linguagem \textsc{COQ}. Para esse trabalho, vamos usar a \textit{functional pearl} \qt{A Play on Regular Expressions} \cite{fischer2010play}. Nesse artigo, os autores apresentararam o algoritmo em três atos, cada qual subdividido em 2 ou 3 cenas. Cada cena descreve uma implementação diferente para expressões regulares, que aumenta, gradualmente em nível de complexidade e generalidade.

O primeiro ato divide-se em duas cenas, a primeira busca descrever os \texttt{regexes} como \texttt{ADTs}, a cena seguinte trata de adicionar pesos à estrutura desenvolvida na primeira parte via \texttt{semirings}. O segundo ato busca implementar o algoritmo de Glushkov para \texttt{matchings} mais eficientes e adiciona a funcionalidade de computar o maior matching à esquerda. O último ato adiciona \texttt{lazyness} na versão do ato anterior. Ao final do artigo original de \cite{fischer2010play} tem-se uma implementação elegante em Haskell. O algoritmo em questão não apenas conseguia competir em termos de eficiência com implementações profissionais em \texttt{C++}, mas também era mais genérico, podendo resolver problemas como a contagem de \texttt{matchings} de uma \texttt{string} com um \texttt{regex} ou capturar o menor \texttt{matching} à esquerda.

\section{Implementação do primeiro ato}

O artigo começa com uma implementação básica para expressões regulares, que pode feita através de uma ADT. Dado um regex $r$, dizemos que uma string $s$ é aceita por $r$ nos seguintes casos:

\begin{figure}[H]
 \begin{minipage}[t]{0.3\textwidth}
  \centering
    \inputminted[firstline=5, lastline=12]{haskell}{code/haskell/src/RegexSimple.hs}
    \vspace{2\baselineskip}
 \end{minipage}
 \begin{minipage}[t]{0.7\textwidth}
  \centering
  \begin{minted}{bash}
    ghci> let nocs = Reg (Alt (Sym 'a') (Sym 'b'))
    ghci> let onec = Seq nocs (Sym 'c')
    ghci> let evencs = Seq (Rep (Seq onec onec)) nocs
    ghci> accept evencs "cc"
    True
    ghci> accept evencs "ccabab"
    True
  \end{minted}
 \end{minipage}
 \captionof{listing}{Regex simples em Haskell.}
\end{figure}

\begin{itemize}
    \item Se $r = \varepsilon$, então $c = \varepsilon$.
    \item Se $s = c$, onde $c$ é um caratere, então $s = c$.
    \item Se $r = (p \mid q)$, onde $p$ e $q$ são \texttt{regexes}, a união é aceita se pelo menos uma das partes é aceita por $r$.
    \item Se $r = (p q)$, onde $p$ e $q$ são \texttt{regexes}, a concatenação é aceita se ambas as partes são aceitas por $r$.
    \item Se $r = u^\ast$, o fecho de Kleene é aceito se $s$ é aceito zero ou mais vezes.
\end{itemize}

\begin{listing}[ht]
\inputminted[firstline=14, lastline=20]{haskell}{code/haskell/src/RegexSimple.hs}
\caption{Matching simples em Haskell.}
\end{listing}
as funções \texttt{split} e \texttt{parts} podem ser consultadas no Apêndice~\ref{anexo-utils}. Na segunda cena, a adição de \texttt{semirings} à estrutura original permite que não só se tenha um valor booleano como resposta (caso uma string tenha \texttt{match} ou não), mas a quantidade de \texttt{matchings} que ocorrem (valor inteiro).

\begin{figure}[H]
 \begin{minipage}[t]{0.3\textwidth}
  \centering
    \inputminted[firstline=7, lastline=14]{haskell}{code/haskell/src/RegexWeighted.hs}
    \vspace{2\baselineskip}
 \end{minipage}
 \begin{minipage}[t]{0.7\textwidth}
  \centering
  \begin{minted}{bash}
    ghci> let as = Alt (Sym 'a') (Rep (Sym 'a'))
    ghci> acceptW (weighted as) "a" :: Int
    2
    ghci> let bs = Alt (Sym 'b') (Rep (Sym 'b'))
    ghci> acceptW (weighted (Seq as bs)) "ab" :: Int
    4
    ghci> acceptW (weighted (Seq as bs)) "ab" :: Bool
    True
  \end{minted}
 \end{minipage}
 \captionof{listing}{Regex com pesos em Haskell.}
\end{figure}

A versão anterior pode ser alterada com duas pequenas mudanças nos tipos báscisp (\texttt{Eps} e \texttt{Sym}). O nova versão de \texttt{Sym} deverá tomar um predicado que compara dois símbolos e retorna o $\bar{0}$ ou $\bar{1}$ de um \texttt{semiring}. Não apenas isso, deve-se implementar um método que transforme um \texttt{regex} normal numa versão com pesos.

\begin{figure}[H]
 \begin{minipage}[t]{0.5\textwidth}
  \centering
    \inputminted[firstline=3, lastline=10]{haskell}{code/haskell/src/Semiring.hs}
    \inputminted[firstline=24, lastline=29]{haskell}{code/haskell/src/RegexWeighted.hs}
    \vspace{2\baselineskip}
 \end{minipage}
 \begin{minipage}[t]{0.5\textwidth}
  \centering
    \inputminted[firstline=16, lastline=22]{haskell}{code/haskell/src/RegexWeighted.hs}
    \vspace{2\baselineskip}
 \end{minipage}
 \captionof{listing}{Semirings e conversão de um Regex normal em Haskell.}
\end{figure}

A nova versão da função \texttt{accept} mantém a mesma forma da definição original. Contudo, os operadores agora são mais genéricos, pois fazem operações em \texttt{semirings}.

\begin{listing}[ht]
\inputminted[firstline=30, lastline=50]{haskell}{code/haskell/src/RegexWeighted.hs}
\caption{Accept com pesos em Haskell.}
\end{listing}

\subsection{Expressões regulares em \textsc{COQ}}

A representação de um regex simples é similar à utilização de ADTs em Haskell, contudo em \textsc{COQ} é possível utilizar Tipos Indutivos, que são uma generalização dos ADTs. A palavra vazia é representada por \texttt{Eps}, um símbolo $c$ (onde $c$ é tipo \texttt{ascii}) é representado por uma função \texttt{Sym c} que toma como argumento um caractere e retorna um regex. Combinações \texttt{Alt} e \texttt{Seq} são codificadas como funções que tomam dois argumentos (do tipo regex) e retornam um regex, o fecho de Kleene é uma função de um argumento que recebe e retorna um regex.

\begin{listing}[ht]
\inputminted[firstline=12, lastline=16]{coq}{code/rpc/src/Simple.v}
\caption{Definição indutiva de uma expressão regular simples.}
\end{listing}

Como \textsc{COQ} não possui compreensão de listas por padrão, as funções polimórificas \texttt{split} e \texttt{parts} foram implementadas usando funções de ordem superior. Definições auxiliares (como \texttt{non\_empty} e \texttt{athead}) podem ser consultadas no Anexo~\ref{anexo-utils}.

\begin{listing}[ht]
\inputminted[firstline=4, lastline=10]{coq}{code/rpc/src/Utils.v}
\inputminted[firstline=24, lastline=31]{coq}{code/rpc/src/Utils.v}
\caption{Funções auxiliares para corte e partição de listas polimórficas.}
\end{listing}
A definição do matching em expressões regulares simples também é similar à solução em Haskell. Utilizando, novamente, high order functions ao invés de compreensão de listas. Definições extras usadas na funções podem ser consultadas no Anexo~\ref{anexo-accept}.
\inputminted[firstline=31, lastline=48]{coq}{code/rpc/src/Simple.v}

\subsection{Semirings}

Diferentemente da implementação em Haskell, o sistema de tipos usado em \textsc{COQ} força que algo seja uma instância de Semiring apenas se satifazer todas as restrições, confirmadas através de provas. As provas de que os tipos Booleanos e Naturais como Semirings são triviais e podem ser consultadas no Anexo~\ref{anexo-semiring}.

\inputminted[firstline=1, lastline=33]{coq}{code/rpc/src/Semiring.v}

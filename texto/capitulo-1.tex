% ----------------------------------------------------------
\chapter{Fundamentação Teórica}
% ----------------------------------------------------------
\cblkqt{Aristóteles formulou seus silogismos na antiguidade e William de Ockham estudou lógica na idade média. Contudo, a disciplina de lógica moderna começou com Gottlob Frege e seu Begriffschrift, escrito em 1879 quando o mesmo tinha 31 anos.}{Philip Wadler \cite{wadler2000proofs}}

\section{A lógica do séc.XIX e a crise do séc.XX}

Friedrich Ludwig Gottlob Frege (1848 - 1925) foi um matemático, lógico e filósofo alemão que trabalhou na Universidade de Jena. Fregue essencialmente reestruturou a disciplina da Lógica ao construir um sistema formal que deu origem ao cálculo de predicados. Em seu sistema formal, Frege desenvolveu a análise de proposições quantificadas e formalizou a noção de \qt{Prova} em termos que ainda são aceitos atualmente. Frege então demonstrou que seria possível utilizar este sistema para resolver proposições matemáticas teóricas em termos de noções lógicas e matemáticas mais simples. Um dos Axiomas que Frege adicionou ao seu sistema, na tentativa de derivar partes significantes da Matemática via Lógica, acabou sendo incosistente \cite{sep-frege}.

\begin{figure}[ht]
\centering
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=0.5\linewidth]{intro/frege.png}
\end{subfigure}\hfil
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=0.5\linewidth]{intro/russell.png}
\end{subfigure}
\caption[ Frege e Russell ]
{Gottlob Frege e Bertrand Russell}
\end{figure}

O paradoxo foi uma descoberta independente do filósofo inglês Bertrand Russell e acabou demonstrando que o formalismo desenvolvido por Frege era, na verdade, inconsistente. Particurlamente o problema se encontrava no Axioma V, conhecido atualmente como compreensão irrestrita de conjuntos \cite{sep-russell-paradox}. O paradoxo de Russell se origina da ideia de que qualquer condição pode ser usada para determinar um conjunto:

\begin{enumerate}
    \item A notação da compreensão de conjuntos, denotando "O conjunto dos $x$ tal que $\varphi(x)$".
        \[ \{ x \mid \varphi(x) \} \]
    \item Seja $R$ o conjunto $\{x \mid \lnot \varphi(x) \}$, onde $\varphi(x) = x \in x $.
    \item $ R \in R $?
    \item Paradoxo:
        \begin{itemize}
            \item Se $R \in R$, então $R \not \in R$.
            \item Se $R \not \in R$, então $R \in R$.
        \end{itemize}
        ou seja:
        \[ R \in R \iff R \not \in R \]
\end{enumerate}

\subsection{O programa de Hilbert, Gödel e Brouwer}
\label{cap1-hilbert}

As respostas a este paradoxo começaram a surgir no início do século XX. O \textit{Principia Mathematica} de Russell \& Whitehead demonstrou que o formalismo lógico poderia, de fato, expressar boa parte da Matemática. Inspirado por essa visão, David Hilbert e seus colegas em Göttigen propuseram um programa para a construção de um base axiomática para a Lógica e a Teoria dos Conjuntos, de forma a resolver o \textit{Entscheidungsproblem}\footnote{Problema da decisão}, i.e., desenvolver um procedimento \textit{efetivamente calculável} para determinar a verdade ou falsidade de qualquer proposição. Isso, porém, pressupõe a noção de completude: Para cada afirmação $P$, ou $P$ ou sua negação ($\lnot P$) possuem uma prova. O programa liderado por Hilbert foi enfraquecido pelos resultados obtidos por Kurt Gödel em 1930, onde o mesmo provou que a aritmética era incompleta \cite{wadler2015propositions}.

Paralelamente o matemático holandês Luitzen Brouwer desenvolveu o intuicionismo, cuja idéia básica é a de que não se pode afirmar a existência de um objeto matemático a menos que se possa definir um procedimento para construí-lo. Os matemáticos Arend Heyting e Andrey Kolmogorov formalizaram essa ideia de forma idependente nos anos 30. A interpretação BHK\footnote{Brower, Heyting \& Kolmogorov} atribui um tipo diferente de interpretação para o ato de provar. Na lógica clássica a semântica de uma fórmula pode ser analizada através de sua tabela verdade, na lógica intuicionista utiliza-se a ideia de \textit{construção} de uma prova, que pode ser especificada via indução na estrutura das fórmulas \cite{troelstra2003constructivism}:

\begin{itemize}
    \item Uma prova de $A \land B$ é dada por um par de provas $\langle p, q \rangle$, onde $p$ é uma prova de $A$ e $q$ é uma prova de $B$.
    \item Uma prova de $A \lor B$ possui forma $\langle 0, p \rangle$ (sendo $p$ uma prova de $A$) ou $\langle 1, q \rangle$ (onde $q$ é uma prova de $B$).
    \item A prova de $A \to B$ é uma construção $q$ que transformar qualquer prova $p$ de $A$ em uma prova $q(p)$ de $B$.
    \item $\bot$ não possui prova. A prova de $\lnot A$ é uma construção que transforma qualquer prova de $A$ numa prova de $\bot$, i.e., $\lnot A$ é definida como $A \to \bot$.
    \item Uma prova $p$ de $\exists x \in D, \varphi(x)$ é um par $\langle d, q \rangle$, onde $d \in D$ e $q$ é uma prova de $\varphi(d)$.
    \item Uma prova $p$ de $\forall x \in D, \varphi(x)$ é uma função que converte qualquer elemento $d \in D$ numa prova de $\varphi(d)$.
\end{itemize}

\subsection{Church, Turing e Gentzen}

Alonzo Church introduziu o $\lambda$-Cálculo como um novo formalismo para os fundamentos da matemática, muitos o consideram a primeira linguagem de programação, criada décadas antes dos primeiros computadores. Essa formulação também é capaz de representar qualquer função computável. De forma independente (e no mesmo ano) Alan Turing escreveu um artigo sobre a máquina que recebe seu nome, demonstrando que o \textit{Entscheidungsproblem} não é decidível e que sua formulação era equivalente à de Church \cite{wadler2000proofs}.

Um tratamento mais rigoroso sobre o cálculo de Church pode ser encontrando em \cite{hindley2008lambda}, nesta seção procura-se desenvolver algumas noções forma intuitiva. No $\lambda$-Cálculo a noção de computação é expressa através da abstração e aplicação de funções. Dado um conjunto contável de símbolos $X$, um $\lambda$-termo pode ser definido da seguinte forma indutiva como uma:
\begin{itemize}
    \item Variável: Se $e \in X$, então $e$ é um $\lambda$-termo.
    \item Aplicação: Sejam $e_1$ e $e_2$ $\lambda$-termos, então a aplicação $(e_1 \, e_2)$ é um $\lambda$-termo.
    \item Abstração: Dada uma variável $x \in X$ e um $\lambda$-termo $e$, temos que $(\lambda x.e)$ também é um $\lambda$-termo.
\end{itemize}

No $\lambda$-Cálculo temos um universo onde \textit{programas são dados}. A definição de aplicação trabalha com a ideia de que qualquer termo pode ser um programa ou um dado. Podemos aplicar qualquer termo $f$ a qualquer termo $t$, gerando um novo termo $(f \, t)$ (que pode ser lido como uma função $f(t)$). Nas abstrações entende-se por $\lambda x.t$ um programa que, dado $x$ como argumento, retorna $t$ como saída, por exemplo, o programa $(\lambda x.(x\,x))$ toma $x$ como entrada e retorna $(x\,x)$ como saída \cite{baez2010physics}.

Dizemos que $x$ é uma variável \textit{ligada} se $x$ está no escopo de algum $\lambda$, por exemplo ${\color{blue}{x}}$ é ligado em $(\lambda {\color{blue}{x}}.y{\color{blue}{x}})$, a  variável ${\color{red}{y}}$ não está no escopo de nenhum $\lambda$ em $(\lambda x.{\color{red}{y}}x)$ e é dita ser \textit{livre}. O conjunto de todas as variáveis \textit{livres} de um $\lambda$-termo $P$ é denotado por $FV(P)$ \cite{hindley2008lambda}. A semântica dos $\lambda$-termos é definida a partir de regras de redução/conversão:

\begin{itemize}
    \item $\alpha$-conversão: Permite a renomeação de variáveis que estejam no escopo de um $\lambda$.
        \[ (\lambda x.(x x)) \equiv_{\alpha} (\lambda y.(y y )) \]
    \item $\beta$-redução: A aplicação de uma abstração $(\lambda x.t)$ no $\lambda$-termo $s$ deve, necessariamente, retornar um $t$ com todas as ocorrências de $x$ substituidas por $s$ ($t[s/x]$).
        \[ \red{((\lambda x.t) \, s)}{\beta}{t[s/x]} \]
        \begin{example}
          \[\red{(\lambda x.x(xy))\,n}{\beta}{(x(xy))[x/n]} = n(ny)\]
        \end{example}
    \item $\eta$-redução: Elimina abstrações redundantes.
        \[ \red{(\lambda x.(t \, x))}{\eta}{t}, \, x \not \in FV(t) \]
        \begin{example}
          \[\red{(\lambda x.ex)\,e^\prime}{\eta}{e\,e^\prime} \]
        \end{example}
\end{itemize}

O cálculo de Church apresenta inconsistências, inicialmente encontradas por Kleene e Rosser em \cite{kleene1935inconsistency}, sendo depois simplificadas por Haskell Curry \cite{sep-curry-paradox}. Isso levou Church a desenvolver versões tipadas deste sistema formal nos anos 40.

O segundo objetivo do programa de Hilbert foi o estabelecimento da consistência de várias Lógicas, uma Lógica inconsistente pode derivar qualquer fórmula, o que a torna inútil. Em 1935 o alemão Gerhard Gentzen introduziu não apenas uma, mas duas novas formulações para a lógica: Dedução Natural e o Cálculo com Sequentes. Gentzen generalizou a noção de \textit{julgamento}, originalmente desenvolvida por Frege, para incluir a noção de Hipóteses em seus Sequentes, a notação:
\[ \phi_1, \ldots, \phi_n \vdash \psi_1, \ldots, \psi_n \]
significa que a partir das premissas $\phi_1, \ldots, \phi_n$ pelo menos uma das fórmulas $\psi_1, \ldots, \psi_n$ é verdadeira. Se $\Gamma$ e $\Sigma$ são conjuntos de fórmulas, o sequente $\Gamma \vdash \Sigma$ indica que é consequência de todas as fórmulas de $\Gamma$ que pelo menos uma fórmula em $\Sigma$ é verdadeira. Sequentes básicos como $\phi \vdash \phi$ são tomados como axiomas. Em contraste com os sistemas dedutivos de Hilbert, caracterizados por poucas regras de inferência e muitos axiomas, os sistemas de Gentzen possuem apenas um axioma e várias regras de inferências (com os conectivos lógicos).

A originalidade desses sistemas dedutivos está em perceber que regras lógicas devem vir sempre em pares (introdução e eliminação).  Uma regra de introdução especifica sob quais circunstâncias é possível asserir uma fórmula dado um conectivo lógico, enquanto as regras de eliminação especificam como usar os conectivos lógicos para gerar conclusões \cite{wadler2015propositions}.

\begin{figure}[!htb]
\begin{gather*}
    \prftree[r]{$\textsc{Id}$}
    {{\color{blue}{\Gamma}}, {\color{blue}{A}} \vdash {\color{blue}{A}}}\\
    \prftree[r]{$\to I$}
    {{\color{blue}{\Gamma}},{\color{blue}{B}} \vdash {\color{blue}{A}}}
    {{\color{blue}{\Gamma}} \vdash {\color{blue}{B \to A}}}
    \quad
    \prftree[r]{$\to E$}
    {{\color{blue}{\Gamma}} \vdash {\color{blue}{B \to A}}}
    {{\color{blue}{\Delta}} \vdash {\color{blue}{B}}}
    {{\color{blue}{\Gamma}}, {\color{blue}{\Delta}} \vdash {\color{blue}{A}}}\\
    \prftree[r]{$\land I$}
    {{\color{blue}{\Gamma}} \vdash {\color{blue}{A}}}
    {{\color{blue}{\Delta}} \vdash {\color{blue}{B}}}
    {{\color{blue}{\Gamma}}, {\color{blue}{\Delta}} \vdash {\color{blue}{A \land B}}}
    \quad
    \prftree[r]{$\land E_1$}
    {{\color{blue}{\Gamma}} \vdash {\color{blue}{A \land B}}}
    {{\color{blue}{\Gamma}} \vdash {\color{blue}{A}}}
    \quad
    \prftree[r]{$\land E_2$}
    {{\color{blue}{\Gamma}} \vdash {\color{blue}{A \land B}}}
    {{\color{blue}{\Gamma}} \vdash {\color{blue}{B}}}
\end{gather*}
\caption{Dedução Natural de Gentzen.}
\end{figure}

\subsection{O isomorfismo de Curry-Howard}
\label{cap1-curry-horward}

O isomorfismo (ou correspondência) de Curry-Howard extende a interpretação BHK numa relação entre programas e provas. Trata-se de observação que provas na Dedução Natural Intuicionista de Gentzen possuem uma correspondência com com programas escritos no $\lambda$-Cálculo tipado de Church \cite{wadler2015propositions}. Essa relação foi descoberta por Haskell B. Curry e William Howard.

\begin{figure}[!htb]
\begin{gather*}
    \prftree[r]{$\textsc{Id}$}
    {{\color{blue}{\Gamma}}, {\color{red}{x:}}\,{\color{blue}{A}} \vdash {\color{red}{x:}}\, {\color{blue}{A}}}\\
    \prftree[r]{$\to I$}
    {{\color{blue}{\Gamma}}, {\color{red}{x:}}\,{\color{blue}{B}} \vdash {\color{red}{t:}}\,{\color{blue}{A}}}
    {{\color{blue}{\Gamma}} \vdash {\color{red}{\lambda x.t :}}\,{\color{blue}{B \to A}}}
    \quad
    \prftree[r]{$\to E$}
    {{\color{blue}{\Gamma}} \vdash {\color{red}{t :}}\,{\color{blue}{B \to A}}}
    {{\color{blue}{\Delta}} \vdash {\color{red}{u :}}\,{\color{blue}{B}}}
    {{\color{blue}{\Gamma}}, {\color{blue}{\Delta}} \vdash {\color{red}{t \, u :}}\,{\color{blue}{A}}}\\
    \prftree[r]{$\land I$}
    {{\color{blue}{\Gamma}} \vdash {\color{red}{t :}}\,{\color{blue}{A}}}
    {{\color{blue}{\Delta}} \vdash {\color{red}{u :}}\,{\color{blue}{B}}}
    {{\color{blue}{\Gamma}}, {\color{blue}{\Delta}} \vdash {\color{red}{\langle t, u \rangle :}}\,{\color{blue}{A \land B}}}
    \quad
    \prftree[r]{$\land E_1$}
    {{\color{blue}{\Gamma}} \vdash {\color{red}{t :}}\,{\color{blue}{A \land B}}}
    {{\color{blue}{\Gamma}} \vdash {\color{red}{t.\texttt{fst} :}}\,{\color{blue}{A}}}
    \quad
    \prftree[r]{$\land E_2$}
    {{\color{blue}{\Gamma}} \vdash {\color{red}{t :}}\,{\color{blue}{A \land B}}}
    {{\color{blue}{\Gamma}} \vdash {\color{red}{t.\texttt{snd} :}}\,{\color{blue}{B}}}
\end{gather*}
\caption[$\lambda$-Cálculo tipado de Church e Dedução Natural de Gentzen]
{$\lambda$-Cálculo tipado de Church (vermelho), Dedução Natural de Gentzen (azul).}
\end{figure}

A ideia de usar computadores para conduzir uma massiva formalização da matemática é bem antiga, mas só foi aparecer como um projeto bem articulado a partir dos esforços do matemático N.G. de Brujin, em seu Automath de Brujin utilizou o isomorfismo de Curry-Howard para verificar computacionalmente teoremas matemáticos. Nas palavras do próprio de Brujin \cite{de1983automath}:

\newpage

\begin{figure}[!htb]
\centering
\begin{subfigure}{0.25\textwidth}
    \centering
    \includegraphics[width=.7\linewidth]{intro/godel.png}
    \caption{Kurt Gödel}
\end{subfigure}\hfil
\begin{subfigure}{0.25\textwidth}
    \centering
    \includegraphics[width=.63\linewidth]{intro/gentzen.jpg}
    \caption{Gerhard Gentzen}
\end{subfigure}\hfil
\begin{subfigure}{0.25\textwidth}
    \centering
    \includegraphics[width=.68\linewidth]{intro/turing.png}
    \caption{Alan Turing}
\end{subfigure}

\medskip

\begin{subfigure}{0.25\textwidth}
    \centering
    \includegraphics[width=.7\linewidth]{intro/brouwer.jpeg}
    \caption{L. E. J. Brouwer}
\end{subfigure}\hfil
\begin{subfigure}{0.25\textwidth}
    \centering
    \includegraphics[width=.63\linewidth]{intro/heyting.jpg}
    \caption{Arend Heyting}
\end{subfigure}\hfil
\begin{subfigure}{0.25\textwidth}
    \centering
    \includegraphics[width=.6\linewidth]{intro/kolmogorov.jpg}
    \caption{Andrey Kolmogorov}
\end{subfigure}

\medskip

\begin{subfigure}{0.25\textwidth}
    \centering
    \includegraphics[width=.7\linewidth]{intro/church.jpg}
    \caption{Alonzo Church}
\end{subfigure}\hfil
\begin{subfigure}{0.25\textwidth}
    \centering
    \includegraphics[width=.8\linewidth]{intro/curry.jpg}
    \caption{Haskell B. Curry}
\end{subfigure}\hfil
\begin{subfigure}{0.25\textwidth}
    \centering
    \includegraphics[width=.7\linewidth]{intro/howard.jpg}
    \caption{William A. Howard}
\end{subfigure}

\medskip

\begin{subfigure}{0.25\textwidth}
    \centering
    \includegraphics[width=0.65\linewidth]{intro/brujin.jpg}
    \caption{Nicolaas de Bruijn}
\end{subfigure}\hfil
\begin{subfigure}{0.25\textwidth}
    \centering
    \includegraphics[width=.9\linewidth]{intro/girard.jpg}
    \caption{Jean-Yves Girard}
\end{subfigure}\hfil
\begin{subfigure}{0.25\textwidth}
    \centering
    \includegraphics[width=.8\linewidth]{intro/reynolds.jpg}
    \caption{John C. Reynolds}
\end{subfigure}

\medskip

\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=0.4\linewidth]{intro/milner.jpeg}
    \caption{Robin Milner}
\end{subfigure}\hfil
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=.25\linewidth]{intro/hindley.jpeg}
    \caption{J. Roger Hindley}
\end{subfigure}\hfil
\caption[Lógicos, Matemáticos e Cientistas da Computação]
{Durante o séc.XX, lógicos, matemáticos e cientistas da computação chegaram a resultados similares, utilizando abordagens diferentes.}
\label{fig:big-logic-shrine}
\end{figure}

\newpage

\blockquote{O \textit{Automath} é uma linguagem para expressar pensamentos matemáticos de forma detalhada. Não se trata de uma linguagem de programação, embora de se assemelhar com uma. É definido através de uma gramática, todo texto escrito de acordo com suas regras têm garantia de se corresponder a um conceito matemático correto. Ele pode ser usado para expressar boa parte da matemática e admite diferentes fundamentos. As regras são tais que um computador por ser instruído para verificar se os textos escritos na linguagem estão corretos. Textos estes que não estão apenas restritos a provas de teoremas simples; podendo conter teorias matemáticas inteiros, incluindo as regras de inferência usadas em tais teorias.}

Um exemplo real da correspondência de Curry-Howard é o aparecimento de \qt{nomes-com-hífen} na literatura da Teoria dos Tipos.Vez ou outra um lógico motivado por problemas da Lógica e um cientista da computação motivado por problemas práticos acabam descobrindo exatamente o mesmo sistema de tipos (geralmente com o lógico chegando primeiro). A maioria das linguagens funcionais utiliza o sistema de tipos de Hindley-Milner, descoberto pelo lógico Hindley em 1969 e re-rescoberto pelo cientista da computação Milner em 1978. O mesmo ocorreu com os sistemas de Girard-Reynolds, descobertos pelo lógica Jean-Yves Girard em 1972 e re-rescobertos pelo cientista da computação John Reynolds em 1974 \cite{wadler2000proofs}.

Juntos, todos esses projetos ajudaram a concentrar a atenção nas conexões entre lógica, linguagem e matemática. Eles também permitiram que os lógicos desenvolvessem uma consciência explícita da natureza dos sistemas formais e dos tipos de resultados metalógicos e metamatemáticos que provaram ser centrais para a pesquisa nos fundamentos da lógica e da matemática nos últimos cem anos \cite{sep-russell-paradox}.

A Ciência da Computação como disciplina nasceu dos formalismos desenvolvidos durante a crise dos fundamentos da matemática no século passado. A presença de conceitos lógicos ocupa uma parte central da disciplina, chegando a ser descrita como \qt{o cálculo da ciência da computação} \cite{manna1985logical}.

\section{Expressões Regulares}

Warren McCulloch e Walter Pitts em \cite{mcculloch1943logical} desenvolveram modelos para tentar entender como cérebro humano conseguia produzir padrões complexos a partir de células básicas conectadas. Neste trabalho é descrito um modelo bem simplificado de um neurônio, sendo um dos primeiros exemplos do que veio a ser chamado depois de \texttt{redes neurais}.

\newpage

Stephen Kleene, um matemático americano e estudante de Alonzo Church, inspirado pelo trabalho de Warren e Pits descreveu estes modelos em \cite{kleene1951representation} com uma álgebra que chamou de \qt{conjuntos regulares}. A notação usada para expressar esses conjuntos ficou conhecida como \qt{expressões regulares}, os resultados de Kleene, porém, só seriam aplicados na computação com o surgimento do Unix.

\begin{figure}[!htb]
\centering
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=0.5\linewidth]{cap1/warren.jpg}
    \caption{Warren McCulloch}
\end{subfigure}\hfil
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=0.5\linewidth]{cap1/pitts.jpg}
    \caption{Walter Pitts}
\end{subfigure}

\medskip

\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=0.5\linewidth]{cap1/kleene.jpg}
    \caption{Stephen Coole Kleene}
\end{subfigure}\hfil
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=0.6\linewidth]{cap1/thompson.jpg}
    \caption{Ken Thompson}
\end{subfigure}
\caption[McCulloch, Pitts, Kleene e Thompson]
{Entre as primeiras conjecturas de Warren e Pitts, até a primeira implementação por Thompson, se passaram mais de 20 anos}
\end{figure}

O Unix (oficialmente UNIX) é um sistema operacional multi-tarefa, multi-usuário que possui muitas variações ao longo do tempo. O Unix original foi desenvolvido nos laboratórios Bell da AT\&T por Ken Thompson, Dennis Ritchie, entre outros. Thompson publicou em 1968 \qt{Um Algoritmo para buscas via Expressões Regulares} \cite{thompson1968programming}, depois implementando a notação de Kleene no editor de texto padrão do Unix (\texttt{ed}). O trabalho de Thompson inspirou a criação de compiladores para expressões regulares e eventualmente levou à crianção to programa \texttt{grep} em 1973 e sua implementação mais completa (o \texttt{egrep}) em 1979, por Alfred Aho. Outras ferramentas do Unix, como o editor de streams \texttt{sed} e a linguagem \texttt{awk}, também provêm mecanismos para descrição de padrões via expressões regulares.

\begin{table}[ht]
\begin{center}
\begin{tabular}{ | m{2cm} | m{8cm} | }
\hline
Símbolo & Função\\ 
\hline
\texttt{.} & Match em qualquer caractere\\ 
\hline
\texttt{[chars]} & Match em qualquer caractere no escopo dos colchetes\\ 
\hline
\texttt{[\escmeta chars]} & Match em qualquer caractere que não está no escopo dos colchetes\\ 
\hline
\texttt{\escmeta} & Match no início da linha\\ 
\hline
\texttt{\$} & Match no fim de linha\\ 
\hline
\texttt{\bklash w} & Match em qualquer \qt{palavra}, i.e., \texttt{[a-zA-Z0-9\_]}\\ 
\hline
\texttt{\bklash s} & Match em caracteres que representam espaços em branco, i.e., \texttt{[~\bklash f\bklash t\bklash r\bklash n]}\\ 
\hline
\texttt{$~\mid~$} & Match no elemento da esquerda ou da direita\\ 
\hline
\texttt{(expr)} & Agrupa elementos\\ 
\hline
\texttt{?} & Zero ou uma instância do elemento que a precede\\ 
\hline
\texttt{$\ast$} & Zero ou mais instâncias do elemento que a precede\\ 
\hline
\texttt{$+$} & Um ou mais instâncias do elemento que a precede\\ 
\hline
\texttt{$\{ n \}$} & Match em exatas n instâncias de elementos que a precede\\ 
\hline
\texttt{$\{ \min, \}$} & Match em no mínimo n instâncias\\ 
\hline
\texttt{$\{ \min, \max\}$} & Match em qualquer quantidade de instâncias entre $\min$ e $\max$\\ 
\hline
\end{tabular}
\end{center}
\caption[Tabela com metacarateres usados em \textit{Perl Compatible Regular Expressions} nos Unixes.]
{Tabela com metacarateres usados em \textit{Perl Compatible Regular Expressions} nos Unixes \cite{nemeth2017unix}.}
\end{table}

\begin{listing}[!ht]
    \begin{minted}{bash}
        $ echo "the quick brown fox" | sed 's/^/start /'
        start the quick brown fox

        $ echo "<b>Removing</b> those <b>HTML</b> tags." | sed 's/<[^>]*>//g'
        Removing those HTML tags.
    \end{minted}
\caption{Exemplos da utilização de regexes e do editor de streams \texttt{sed}.}
\end{listing}

\section{Alfabetos, Linguagens e Strings}

Um alfabeto é um conjunto finito (e não vazio) de caracteres, usualmente denotado por $\Sigma$. Os elementos de $\Sigma$ são chamados de \qt{símbolos}. Alguns exemplos comuns de alfabetos são:

\begin{itemize}
    \item $\Sigma_{L} = \{a, b, c, \ldots, z, A, B, \ldots, Z\}$ é o alfabeto latino padrão.
    \item $\Sigma_{2} = \{0, 1\}$ é um alfabeto binário.
    \item O conjunto de todos os caracteres \texttt{ASCII}.
\end{itemize}

Uma \texttt{string} (ou \texttt{palavra}) é qualquer sequência finita de símbolos extraídos de um alfabeto específico. Por exemplo, \qt{010101111} é uma string gerada pelos símbolos do alfabeto $\Sigma = \{0,1\}$, a string \qt{1111} também pode ser gerada pelo mesmo alfabeto.

\subsection{Operações em strings}

Uma operação útil quando se lida com strings é poder computar seu comprimento. O \textit{comprimento} é definido pela quantidade de posições pros símbolos de uma string, vale ressaltar que isso não deve ser confundido com a quantidade de símbolos. Por exemplo, a string \qt{1010} possui apenas $2$ símbolos ($0$ e $1$), mas seu comprimento, i.e. a quantidade de posições para estes símbolos, é $4$. A notação padrão para o comprimento de uma string $w$ qualquer é $\len{w}$.

Outra operação usual é poder tomar duas strings arbitrárias $x, y$ pertencentes a um alfabeto $\Sigma$ e poder concatená-las. A \textit{concatenação} de duas strings $x,y$ formadas de símbolos de $\Sigma$, denotada por $xy$, representa a composição dos símbolos de $x = a_1 a_2 \ldots a_{k-1} a_{k}$ com os símbolos de $y = b_1 b_2 \ldots b_{l - 1} b_l$, formando $xy = a_1 a_2 \ldots a_{k-1} a_k b_1 b_2 \ldots b_{l-1} b_l$ de comprimento $k + l$.

Com as definições anteriores é possível trabalhar com a noção de uma \qt{string vazia}, usualmente denotada por $\varepsilon$, $\varepsilon$ é resultado da escolha de $0$ símbolos de qualquer alfabeto $\Sigma$. A string vazia possui propriedades interessantes como:

\begin{itemize}
    \item $\varepsilon$ tem comprimento $0$.
        \[ \len{\varepsilon} = 0 \]
    \item $\varepsilon$ é o elemento neutro da concatenação.
        \[ \varepsilon w = w = w \varepsilon \]
\end{itemize}

Se $\Sigma$ é um alfabeto, pode-se expressar o conjunto de todas as strings com um determinado comprimento usando a notação de exponenciação. Define-se $\Sigma^k$ como o conjunto das strings geradas por $\Sigma$ que possuem comprimento $k$. Por exemplo, dado $\Sigma = \{0,1\}$, tem-se:

\begin{itemize}
    \item $\Sigma^0 = \{ \varepsilon \} $
    \item $\Sigma^1 = \{ 0, 1\} $
    \item $\Sigma^2 = \{ 00, 01, 10, 11 \} $
    \item $\Sigma^3 = \{ 000, 001, 010, 100, 101, 011, 110, 111 \} $
\end{itemize}

O conjunto de todas as strings em um alfabeto $\Sigma$ é denotado por $\kleene{\Sigma}$, se a string vazia for desconsiderada usa-se $\kplus{\Sigma}$. Ambos os casos são representandos por uma união infinita:

\begin{align}
    \kleene{\Sigma} &= \Sigma^0 \cup \Sigma^1 \cup \Sigma^2 \cup \cdots\\
    \kplus{\Sigma} &= \Sigma^1 \cup \Sigma^2 \cup \Sigma^3 \cup \cdots
\end{align}

\subsection{Linguagens Regulares}

Uma linguagem $\mathcal{L}$ sob um alfabeto $\Sigma$ é um conjunto $\mathcal{L} \subseteq \Sigma$. Alguns exemplos abstratos incluem:

\begin{itemize}
    \item $\kleene{\Sigma}$ é uma linguagem sob qualquer alfabeto $\Sigma$.
    \item O conjunto $\{\varepsilon, 01, 10, 0011, 0011, 0101, 1010, \ldots \}$ é uma linguagem sob $\Sigma = \{0,1\}$ que contém todas as strings de $ \kleene{\Sigma} $ com uma quantidade igual de zeros e uns.
    \item O conjunto $\{\varepsilon\}$ consiste apenas da string vazia e é uma linguagem sob qualquer alfabeto $\Sigma$.
    \item A linguagem $\mathcal{D}$ sob of alfabeto $\Sigma = \{ 0, 1\}$ contém todas as sequências infinitas formadas por $0$ e $1$.
    \[ \mathcal{D} = \{ \omega_0 \omega_1 \omega_2 \ldots \mid \omega_i \in \{0, 1\}, \forall i \in \mbb{N} \}\]
\item $\emptyset \subset \kleene{\Sigma}$ é uma linguagem sob qualquer alfabeto $\kleene{\Sigma}$. A \textit{linguagem vazia} $\emptyset$ não contém nenhuma string e não deve ser confundida com $\{ \varepsilon \}$, que contém apenas uma string.
\end{itemize}
adicionalmente, $\mathcal{L} \subset \kleene{\Sigma}$ é dita ser \textit{regular} se obedece a um dos casos \cite{sipser2006introduction}:

\begin{itemize}
    \item $\mathcal{L} = \emptyset$.
    \item $\mathcal{L} = \{\varepsilon\}$.
    \item $\mathcal{L} = \{c\}$, onde $c \in \Sigma$.
    \item $\mathcal{L}$ pode ser construída a partir das seguintes operações:
    \begin{itemize}
        \item A \textit{concatenação} de duas linguagens regulares $L_1$ e $L_2$ sob um alfabeto $\Sigma$ é denotada por:
            \[ L_1 \conc L_2 = \{ wx \in \kleene{\Sigma} \mid w \in L_1 \land x \in L_2 \} \]
            a notação $L^n$, onde $n \geq 0$, representa o seguinte conjunto:
            \[ L^n = \underbrace{L \conc L \conc \cdots \conc L \conc L}_{\text{n vezes}} = \{ w_0 w_1 \ldots w_{n-1} w_{n} \in \kleene{\Sigma} \mid \forall w_i \in L \} \]
            também é possível omitir a operação \qt{$\conc$}, deixando-a implícita, por exemplo:
            \[ L_1 \conc L_2 \equiv L_1 L_2 \]
        \item A \textit{união} de duas linguagens regulares $L_1$ e $L_2$ sob um alfabeto $\Sigma$ é denotada por:
            \[ L_1 \union L_2 = \{ y \in \kleene{\Sigma} \mid y \in L_1 \lor y \in L_2 \} \]
        \item O \textit{fecho de Kleene} de uma linguagem regular $L$, denotado por $\kleene{L}$, representa a seguinte união infinita:
            \[ \kleene{L} = \bigcup_{i=0}^{\infty} L^i \]
    \end{itemize}
\end{itemize}

Na aritmética, podemos usar as operações $+$ e $\times$ para definir expressões como \qt{$(5+3)\times 4$} que, quando computada, tem valor $32$. Similarmente podemos usar o formalismo de Kleene para definir expressões que descrevem linguagens. Expressões regulares são formas compactas de escrever linguagens regulares. Por exemplo, a expressão \qt{$(0 \cup 1) \kleene{0}$} descreve uma linguagem regular $\mathcal{L}$ tal que:

\[ \mathcal{L} = \{0, 1, 00, 10, 000, 100, \ldots \} \]
que contém todos as strings de $\Sigma$ que possuem uma quantidade igual de zeros e uns.

\section{Álgebra Abstrata}

Esta seção é uma adaptação de \cite{pin2010mathematical} e visa definir os \textit{semianéis}, que são usados no algoritmo estudado no próximo capítulo.

\subsection{Magmas, Semigrupos e Monóides}

Os Magmas (ou Grupóides) são estruturas algébricas simples (e pouco divulgadas). Um Magma é definido como um conjunto $X$ equipado com uma operação binária $\aplus : X \times X \to X$ fechada (em $X$). Como exemplos de Magmas pode-se citar:

\begin{itemize}
    \item O conjunto dos Naturais munidos da operação de adição, $(\mbb{N}, +)$, forma um Magma.
    \item Os Naturais munidos da operação de subtração, $(\mbb{N}, -)$, também forma um Magma.
    \item $(\mbb{N}, \min)$ também é um exemplo de um Magma.
\end{itemize}

Apesar de serem extremamente comuns, os magmas não são tão úteis quanto os \textit{semigrupos} ou os \textit{monóides}. Um semigrupo é um conjunto $S$ dotado de uma operação binária fechada ($S \times S \rightarrow S$) e associativa, por conveniência chamada de multiplicação. Exemplos de semigrupos são:

\begin{itemize}
    \item Os inteiros positivos ($\mbb{Z}^+$) munidos da operação de adição: $(\mbb{Z}^+, +)$.
    \item O conjunto com valores booleanos $\mbb{B} = \{\top, \bot\}$ munido com as operações de disjunção ou conjunção: $(\mbb{B}, \lor)$ ou $(\mbb{B}, \land)$.
    \item O conjunto das \texttt{strings} dotado de uma operação de concatenação (\texttt{+}) também forma um semigrupo:
        \[ \texttt{\qt{hello}} + (\texttt{\qt{, }} + \texttt{\qt{world}}) = (\texttt{\qt{hello}} + \texttt{\qt{, }}) + \texttt{\qt{world}} \]
\end{itemize}

Um monóide é uma estrutura $(X, \aplus, \bar{1})$ onde $(X, \aplus)$ é um semigrupo contendo um elemento neutro $\bar{1} \in X$ com a seguinte propriedade:

\[ \forall x \in X, \bar{1} \aplus x = x \aplus \bar{1} = x \]

\begin{itemize}
    \item O conjunto dos números naturais munidos da operação de adição ou de multiplicação tendo, respectivamente, $0$ e $1$ como elementos neutros forman os seguintes monóides: $(\mbb{N}, +, 0)$ e $(\mbb{N}, \cdot, 1)$.
    \item As \texttt{strings} dotadas de uma operação de concatenação e tendo a \texttt{string} vazia $\varepsilon$ como elemento neutro também formam um monóide.
        \[ \texttt{s} + \varepsilon = \varepsilon + s = s \]
\end{itemize}

\subsection{Semianéis}

Um semianel é uma estrutura consistindo de um conjunto $X$, duas operações binárias $\aplus, \amul : X \times X \to X$ e dois elementos neutros $\bar{0}, \bar{1}$. As estruturas $(X, \aplus, \bar{0})$ e $(X, \amul, \bar{1})$ também são monóides e devem obedecer as seguintes restrições:

\begin{itemize}
    \item Comutatividade da adição:
        \[ a \aplus b = b \aplus a \]
    \item O elemento $\bar{0}$ aniquila a multiplicação:
        \[ a \amul \bar{0} = \bar{0} \amul a = \bar{0} \]
    \item Distributividade da multiplicação e adição:
        \begin{align*}
            a \amul (b \aplus c) &= (a \amul b) \aplus (a \amul c) \\
            (a \aplus b) \amul c &= (a \amul b) \aplus (a \amul c)
        \end{align*}
\end{itemize}

\section{Testes baseados em propriedades}

John Hughes e Koes Clessen descrevem em \cite{claessen2011quickcheck} que o custo para escrita de testes motiva esforços para automatizá-los. O resultado de seu trabalho conjunto foi o QuickCheck, uma biblioteca em Haskell para geração de testes aleatórios a partir de propriedades.

Neste tipo de ferramenta o programador deve definir a especificação de um programa, através da criação de propriedades que devem ser satisfeitas (usando combinadores disponibilizados pela biblioteca), que são então usados pelo QuickCheck para fazer para gerar analisar a distribuição dos dados, gerar automaticamente testes e, quando encontrado um contra-exemplo à propriedade, minimizá-lo de forma a encontrar um contra-exemplo mínimo (e mais fácil de ser entendido).

\begin{figure}[H]
  \centering
  \begin{tikzpicture}[node distance=2cm]
    \node (seed) [decision] {Seed};
    \node (size) [decision, below of=seed] {Size};
    \node (GenA) [startstop, right of=seed, xshift=2cm] {Gen A};
    \node (A) [process, right of=GenA, xshift=2cm] {A};

    \draw [arrow] (seed) -- node[anchor=south] {} (GenA);
    \draw [arrow] (size) -- node[anchor=east] {} (GenA);
    \draw [arrow] (GenA) -- node[anchor=south] {} (A);
  \end{tikzpicture}
  \caption{Estrutura de um Gerador \texttt{Gen A}, que gera elementos do tipo \texttt{A}.}
\end{figure}

Ao lidarmos com testes baseados em propriedades não podemos assumir que receberemos tudo de graça. O problema agora passa da criação e escrita de bons casos de testes para a preocupação com o desenvolvimentos de geradores e boas propriedades que devem ser obedecidas. Geradores são primitivas oferecidas pela biblioteca QuickChick e podem ser compostos de forma gerar estruturas comuns (como listas de tipos de primitivos) ou customizadas (estruturas definidas pelo próprio usuário).

Como exemplo, tome as seguintes primitivas: \texttt{arbirtrary} e \texttt{elements}. A primeira tem como entrada um tipo $A$ e retorna elementos arbirtrários deste tipo. A segunda tem como entrada uma lista, que define um intervalo para as saídas possíveis. A primitiva \texttt{sample} faz uma amostragem e toma um gerador como entrada.

\begin{figure}[H]
 \begin{minipage}[t]{0.5\textwidth}
  \centering
  \begin{minted}[gobble=0, mathescape, fontsize=\tiny]{bash}
*Main> import Test.QuickCheck
*Main Test.QuickCheck> :t sample
sample :: Show a => Gen a -> IO ()
*Main Test.QuickCheck> sample (arbitrary :: Gen Int)
0
0
-2
1
7
6
-7
0
-7
-10
-14
  \end{minted}
 \end{minipage}
 \begin{minipage}[t]{0.5\textwidth}
  \centering
  \begin{minted}[gobble=0, mathescape, fontsize=\tiny]{bash}
*Main Test.QuickCheck> :t elements
elements :: [a] -> Gen a
*Main Test.QuickCheck> sample $ elements [1..100]
87
59
5
50
25
23
10
29
53
81
63
  \end{minted}
 \end{minipage}
 \captionof{listing}{Geração de inteiros arbitrários usando as primitivas \texttt{arbitrary} e \texttt{elements}.}
\end{figure}

Assim como em testes normais, é necessário que se tome cuidado com a eficiência de cada teste. O exemplo abaixo demonstra a utilizadação da primitiva \texttt{suchThat}, que toma gera elementos obedecendo um predicado. Utilizamos um exemplo extramamente ineficiente, pois inteiros aleatórios serão gerados e descartados se não obedecerem nosso predicado. O segundo exemplo usa o fato dos geradores serem instâncias de um \texttt{functor} ($\langle \$ \rangle$), os inteiros não são mais descartados na aplicação da função $(\lambda x.(3000 + \texttt{abs x}))$.

\begin{figure}[H]
 \begin{minipage}[t]{0.5\textwidth}
  \centering
  \begin{minted}[gobble=0, mathescape, fontsize=\tiny]{bash}
*Main Test.QuickCheck> :set +s
*Main Test.QuickCheck> sample \
                      (arbitrary `suchThat` (>=3000))
3001
3014
3002
3000
3003
3005
3009
3017
3007
3008
3002
(7.18 secs, 11,296,080,304 bytes)
  \end{minted}
 \end{minipage}
 \begin{minipage}[t]{0.5\textwidth}
  \centering
  \begin{minted}[gobble=0, mathescape, fontsize=\tiny]{bash}
*Main Test.QuickCheck> sample $ \
                      (\x -> (3000 + abs x)) <$> arbitrary
3000
3000
3001
3003
3007
3008
3012
3011
3001
3014
3015
(0.01 secs, 592,040 bytes)
  \end{minted}
 \end{minipage}
 \captionof{listing}{Comparação do desempenho da versão ingênua (esquerda) com a versão que utiliza \texttt{functors} (direita).}
\end{figure}

Devido ao sucesso do QuickCheck outras linguagens começaram a implementá-lo. Recentemente o Coq ganhou seu próprio framework para geração de testes baseados em propriedades, o QuickChick \cite{denes2014quickchick}, que implementa o protocolo original do QuickCheck.

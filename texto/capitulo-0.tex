% ----------------------------------------------------------
\chapter{Introdução}
% ----------------------------------------------------------
\cblkqt{\textbf{Perlis:} Boa parte da complexidade de um programa é espúria, vários casos testes podem ser estudados para exaurir o problema. A dificuldade trata-se apenas de isolar os melhores casos de teste, não em provar o algoritmo, pois isso segue da escolha dos melhores casos de testes.\\
\textbf{Dijkstra:} Testes apenas demonstram a presença, não a ausência, de bugs.}{\cite{randell19961968}}

Os métodos formais são uma área da computação focada em utilizar computadores para ajudar na especificação de construção de software e hardware. Esta metodologia é extremamente importante na área de computação distribuída, visto que certos protocolos são de natureza complexa, sendo inviável sua implementação sem alguma especificação detalhada.

Testes são uma das maneiras mais básicas de se garantir a robustez de um software, a qualidade de um software pode estar atrelada a quão bem escritos estão seus testes. Uma base de código que não é testada fica cada vez mais difícil de manter e modificar com o passar do tempo, dessa forma, testar não é só importante, torna-se algo necessário. Contudo, como qualquer código, testes exigem manutenção, testes ruins ou lentos podem tornar uma integração contínua inviável. Portanto, é desejável que se escrevam testes mais robustos (que testam mais) com a menor quantidade de código possível.

De forma geral, um projeto de software genérico possuirá três níveis: testes unitários, testes de integração e testes de sistema \cite{dooley2011software}. Testes unitários por sí só não garantem a corretude de um sistema como um todo, apenas de um determinado módulo. Deve-se também garantir que os testes do módulo foram bem pensandos, i.e., eles testam situações inusitadas ou casos pouco comuns. A responsabilidade de escrever bons testes fica a cargo da equipe de desenvolvimentos. Os testes de integração buscam garantir que a comunicação entre os módulos está sendo feita de forma correta, i.e., os testes são escritos com informações providas pelas interfaces, sendo desnecessário conhecer como os módulos estão implementados. O testes de sistema verificam se o sisteam está completamente integrado de acordo com o que é esperado.

Como já foi enfatizado na citação de Dijkstra, testes não garantem a ausência de bugs, apenas uma prova formal pode fazê-lo. Contudo, provas matemáticas da corretude de software podem levar tempo e exigem um tipo de conhecimento muito específico. Nesse ponto, a utilização de testes baseados em propriedades podem ser uma boa alternativa, pois não só oferecem garantias melhores que as providas por testes unitários.

\section{Objetivos}

A plataforma escolhida para este trabalho é o assistente de provas Coq \cite{the_coq_development_team_2019_2554024}, que foi criado para desenvolver provas matemáticas, escrever especificações formais de programas e verificar sua corretude com respeito à especificação. A linguagem usada para escrever as provas chama-se Gallina\footnote{Um trocadilho com Coq, que significa \qt{Galo} em francês}. Os termos dessa linguagem podem representar programas, propriedades e provas de propriedades. Usando o Isomorfismo de Curry-Howard (seção \ref{cap1-curry-horward}) programas, propriedades e provas podem ser formalizadas na mesma linguagem, conhecida como \qt{Cálculo de Construções Indutivas}, trata-se de um $\lambda$-Cálculo tipado de ordem superior, inicialmente investigado em \cite{coquand1986calculus}. A lógica central é de natureza intuicionista (seção \ref{cap1-hilbert}), regras como terceiro excluído ($\vdash P \lor \lnot P$) ou eliminação da dupla negação ($\lnot \lnot P \vdash P$) não valem naturalmente. Alguns projetos de notoriedade feitos em \textsc{COQ} incluem:

\begin{itemize}
  \item CompCert \cite{leroy2012compcert}: Um compilador para um subconjunto da linguagem C formalmente verificado, o código gerado em assembly tem garantias de seguir o comportamento descrito pelo código fonte em C.
  \item Uma prova formal do famoso teorema das 4 cores \cite{gonthier2008formal}.
  \item CertiKOS \cite{gu2011certikos}: Uma arquitetura para construção de kernels concorrentes.
  \item FSCQ \cite{chen2017verifying}: Um sistema de arquivos formalmente verificado.
  \item O projeto UniMath \cite{unimath}: Uma biblioteca que visa formalizar um corpo substancial da matemática sob o ponto de vista univalente.
\end{itemize}

Recentemente, o ecosistema do \textsc{COQ} ganhou uma implementação do QuickCheck \cite{claessen2011quickcheck} denominada QuickChick. O QuickCheck é um protocolo originalmente implementado em Haskell e também trata-se da primeira biblioteca para testes baseados em propriedaes. A utilização de uma biblioteca para testes num provador de teoremas pode parecer estranha, mas é extremamente útil quando lidados com conjecturas, se um caso que as falsifique for encontrado então podemos usar essa informação para melhorar nossas definições ou tentar entender melhor o problema, evitando assim a perda de tempo numa prova impossível.

\subsection{Objetivo geral}

\begin{itemize}
  \item Investigar os fundamentos matemáticos da criação de software correto por construção.
\end{itemize}

\subsection{Objetivos específicos}

\begin{itemize}
  \item Implementar o algoritmo descrito no artigo \qt{A play on regular expressions} em Coq.
  \item Implementar geradores e verificar a robustez da implementação em \textsc{COQ} usando testes baseados em propriedades da biblioteca QuickChick.
\end{itemize}

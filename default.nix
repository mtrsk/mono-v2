{ pkgs ? import ./pinned-nixpkgs.nix { json = ./.nixpkgs-version.json; }}:

with pkgs;

let
  texliveEnv = texlive.combine {
    inherit (texlive)
      beamer
      beamertheme-metropolis
      scheme-medium
      abntex2
      enumitem
      ifetex
      ifxetex
      latexmk
      listings
      memoir
      minted
      natbib
      pgf
      prftree
      pygmentex
      python
      textcase
      collection-basic
      collection-fontsextra
      collection-fontsrecommended
      collection-langenglish
      collection-langportuguese
      collection-latex
      collection-latexextra
      collection-mathscience
      hyphen-portuguese
      ;
  };
  extraDeps = [
    lmodern
	pygmentex
    python37Packages.pygments
    python37Packages.pillow
    python37Packages.xstatic-pygments
  ];
in
stdenv.mkDerivation rec {
  name = "latex-env";
  src = ./.;
  buildInputs = [ texliveEnv ] ++ extraDeps;
  installPhase = ''
    runHook preInstall
    mkdir $out
    cp projeto.pdf $out
    runHook postInstall
  '';
}

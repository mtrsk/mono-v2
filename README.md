# Monografia

## Building

* With Nix
    ```
    $ nix-build -v
    ```
* With Docker
    ```
    $ docker image build -t latex-doc .
    ```
